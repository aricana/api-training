package nl.capgemini.testing.campgemini.helpers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class JSONfileReader {

    private static JSONfileReader jsoNfileReader;

    private static ObjectMapper objectMapper = new ObjectMapper();

    private JSONfileReader() {

    }

    public static JSONfileReader getInstance() {
        if (jsoNfileReader == null) {
            jsoNfileReader = new JSONfileReader();
        }
        return jsoNfileReader;
    }

    public Map<String, String> readConfig(String path) throws IOException {
        return objectMapper.readValue(
                new File(path),
                new TypeReference<Map<String, String>>() {
                });
    }

    public ArrayList<JsonNode> getJsonArray(String path) throws IOException {
        return objectMapper.readValue(new File(path), new TypeReference<ArrayList<JsonNode>>() {
        });
    }

    public Map getObjectFromJson(String path) throws IOException {
        return objectMapper.readValue(new File(path), Map.class);
    }

    public Object readJson(String path, Class<?> clazz) throws IOException {
        return objectMapper.readValue(new File(path), clazz);
    }


}
