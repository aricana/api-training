package nl.capgemini.testing.campgemini.configuration;

import org.hsqldb.util.DatabaseManagerSwing;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Profile("default")
public class ServerWindowInitializer  {

    @PostConstruct
    public void startDBManager() {
        DatabaseManagerSwing.main(new String[]{"--url", "jdbc:hsqldb:mem:testdb", "--user", "sa", "--password", ""});
    }

}
