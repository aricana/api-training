package nl.capgemini.testing.campgemini.controllers;

import nl.capgemini.testing.campgemini.database.CatalogueItem;
import nl.capgemini.testing.campgemini.database.CatalogueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
public class FrontEndController {

    private final CatalogueRepository catalogueRepository;



    @Autowired
    public FrontEndController(final CatalogueRepository catalogueRepository) {
        this.catalogueRepository = catalogueRepository;
    }

    @GetMapping({"/", "/catalogue"})
    public String catalogue(final Model model) {
        final List<CatalogueItem> catalogueList = StreamSupport
                .stream(catalogueRepository.findAll().spliterator(), false)
                .filter(item -> item.getSupply() != null)
                .collect(Collectors.toList());
        model.addAttribute("catalogue", catalogueList);
        return "catalogue-view";
    }

    @GetMapping("/catalogue/{id}")
    public String item(final Model model, @PathVariable("id") final long id) {
        final Optional<CatalogueItem> item = catalogueRepository.findById(id);
        if (!item.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        model.addAttribute("item", item.get());
        return "item-view";
    }
}
