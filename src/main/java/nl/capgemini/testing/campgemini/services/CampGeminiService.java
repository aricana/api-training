package nl.capgemini.testing.campgemini.services;

import nl.capgemini.testing.campgemini.database.CatalogueItem;
import nl.capgemini.testing.campgemini.database.CatalogueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CampGeminiService {

    private final CatalogueRepository catalogueRepository;

    @Autowired
    public CampGeminiService(final CatalogueRepository catalogueRepository) {
        this.catalogueRepository = catalogueRepository;
    }

    public ResponseEntity<?> createNewItem(final CatalogueItem newItem) {
        if (newItem.getSupply() == null) {
            newItem.setSupply(0);
        }
        catalogueRepository.save(newItem);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


    public ResponseEntity<?> retrieveCatalogueList() {
        final List<CatalogueItem> result = StreamSupport
                .stream(catalogueRepository.findAll().spliterator(), false)
                .filter(item -> item.getSupply() != null)
                .peek(item -> item.setDescription(null)
                        .setSupply(null)
                ).collect(Collectors.toList());
        return ResponseEntity.ok(result);
    }


    public ResponseEntity<?> retrieveItem(final long id) {
        final Optional<CatalogueItem> result = catalogueRepository.findById(id);
        if (!doesCatalogueItemExist(result)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(result.get());
    }

    private boolean doesCatalogueItemExist(final Optional<CatalogueItem> itemOptional) {
        return itemOptional.isPresent() && itemOptional.get().getSupply() != null;
    }

    public ResponseEntity<?> updateExistingItem(final long id, final CatalogueItem newItem) {
        final Optional<CatalogueItem> result = catalogueRepository.findById(id);
        if (!doesCatalogueItemExist(result)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            CatalogueItem toUpdate = getAndUpdateItem(id, newItem);
            catalogueRepository.save(toUpdate);
            return ResponseEntity.accepted().build();
        }

    }

    private CatalogueItem getAndUpdateItem(long id, CatalogueItem newItem) {

        CatalogueItem result = StreamSupport
                .stream(catalogueRepository.findAll().spliterator(), false)
                .filter(item -> item.getId() == id)
                .findFirst().orElseThrow(NullPointerException::new);
           return  CatalogueItem.builder(result).withItem(newItem).build();

        }



    public ResponseEntity<?> buyItem(final long id, final Integer amount) {
        final int tempAmount = amount == null ? 1 : amount;
        final Optional<CatalogueItem> itemOptional = catalogueRepository.findById(id);
        if (!doesCatalogueItemExist(itemOptional)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        final CatalogueItem catalogueItem = itemOptional.get();
        if (tempAmount > catalogueItem.getSupply()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body("Only " + catalogueItem.getSupply() + " available");
        }
        catalogueItem.setSupply(catalogueItem.getSupply() - tempAmount);
        catalogueRepository.save(catalogueItem);
        return ResponseEntity.ok(catalogueItem);
    }

    public ResponseEntity<?> deleteItem(final long id) {
        catalogueRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<?> deleteAllItems() {
        catalogueRepository.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
